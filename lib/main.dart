import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Favorite Auto',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF242640),
      appBar: AppBar(
        backgroundColor: Color(0xFF242640),
        centerTitle: true,
        title: Text('Fast Cars'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(top: 44, bottom: 32),
              child: Image.asset('images/supra.png'),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 20),
              child: Text(
                'Toyota Supra',
                style: GoogleFonts.racingSansOne(
                  textStyle: TextStyle(color: Colors.white, fontSize: 28),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Text(
                "The Toyota Supra (トヨタ・スープラ) is a sports car and grand tourer manufactured by Toyota Motor Corporation. The initial four generations of the Supra were produced from 1978 to 2002. The fifth generation has been produced since March 2019.",
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Icon(
                        Icons.sports_motorsports_outlined,
                        color: Colors.white,
                      ),
                      Text(
                        'Speed',
                        style: GoogleFonts.lato(
                          textStyle: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                      Text(
                        '250 km/h',
                        style: GoogleFonts.lato(
                          textStyle: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Icon(Icons.speed_outlined, color: Colors.white),
                      Text(
                        '0 - 100',
                        style: GoogleFonts.lato(
                          textStyle: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                      Text(
                        '4.3 sec',
                        style: GoogleFonts.lato(
                          textStyle: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Icon(Icons.monetization_on_outlined, color: Colors.white),
                      Text(
                        'Price',
                        style: GoogleFonts.lato(
                          textStyle: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                      Text(
                        '51000\$',
                        style: GoogleFonts.lato(
                          textStyle: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
